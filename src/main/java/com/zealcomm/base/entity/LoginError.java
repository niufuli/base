package com.zealcomm.base.entity;

public class LoginError {


    /**
     * code : 1001
     * message : Non-exist user
     */

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
