package com.zealcomm.base.entity;

import java.util.List;

public class GroupData {


    /**
     * _id : 5f2a52af562df77f2ff12c97
     * weight : 1
     * userId : 5f2a51ac58ae5b2dc8bda347
     * groupId : 5f2a529e562df77f2ff12c94
     * createdBy : 5f2a518b58ae5b2dc8bda346
     * createdAt : 2020-08-05T06:33:19.284Z
     * __v : 0
     * group : {"_id":"5f2a529e562df77f2ff12c94","name":"111","org":"jackie","createdBy":"5f2a518b58ae5b2dc8bda346","createdAt":"2020-08-05T06:33:02.048Z","formTemplates":[],"__v":0}
     */

    private String _id;
    private int weight;
    private String userId;
    private String groupId;
    private String createdBy;
    private String createdAt;
    private int __v;
    private GroupBean group;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public GroupBean getGroup() {
        return group;
    }

    public void setGroup(GroupBean group) {
        this.group = group;
    }

    public static class GroupBean {
        /**
         * _id : 5f2a529e562df77f2ff12c94
         * name : 111
         * org : jackie
         * createdBy : 5f2a518b58ae5b2dc8bda346
         * createdAt : 2020-08-05T06:33:02.048Z
         * formTemplates : []
         * __v : 0
         */

        private String _id;
        private int id ;
        private String name;
        private String org;
        private String createdBy;
        private String createdAt;
        private int __v;
        private List<?> formTemplates;
        private String defaultPolicy;
        private String updatedAt;
        private String updatedBy;
        private List<?> customizedPolicy;
        private String businessFlowId;
        private List<?> workflowSteps;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrg() {
            return org;
        }

        public void setOrg(String org) {
            this.org = org;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }

        public List<?> getFormTemplates() {
            return formTemplates;
        }

        public void setFormTemplates(List<?> formTemplates) {
            this.formTemplates = formTemplates;
        }

        public List<?> getWorkflowSteps() {
            return workflowSteps;
        }

        public void setWorkflowSteps(List<?> workflowSteps) {
            this.workflowSteps = workflowSteps;
        }

        public List<?> getCustomizedPolicy() {
            return customizedPolicy;
        }

        public void setCustomizedPolicy(List<?> customizedPolicy) {
            this.customizedPolicy = customizedPolicy;
        }

        public String getDefaultPolicy() {
            return defaultPolicy;
        }

        public void setDefaultPolicy(String defaultPolicy) {
            this.defaultPolicy = defaultPolicy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(String updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getBusinessFlowId() {
            return businessFlowId;
        }

        public void setBusinessFlowId(String businessFlowId) {
            this.businessFlowId = businessFlowId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
