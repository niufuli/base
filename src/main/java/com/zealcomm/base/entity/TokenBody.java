package com.zealcomm.base.entity;

public class TokenBody {


    /**
     * data : {"_id":"5e6e0bfec30c11000e5f7b71","org":"1","userName":"1","role":"agent","__v":0,"isDisabled":"N","id":"5e6e0bfec30c11000e5f7b71"}
     * iat : 1595555703
     * exp : 1595598903
     */

    private DataBean data;
    private int iat;
    private int exp;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getIat() {
        return iat;
    }

    public void setIat(int iat) {
        this.iat = iat;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public static class DataBean {
        /**
         * _id : 5e6e0bfec30c11000e5f7b71
         * org : 1
         * userName : 1
         * role : agent
         * __v : 0
         * isDisabled : N
         * id : 5e6e0bfec30c11000e5f7b71
         */

        private String _id;
        private String org;
        private String userName;
        private String role;
        private int __v;
        private String isDisabled;
        private String id;
        private Profile profile;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getOrg() {
            return org;
        }

        public void setOrg(String org) {
            this.org = org;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }

        public String getIsDisabled() {
            return isDisabled;
        }

        public void setIsDisabled(String isDisabled) {
            this.isDisabled = isDisabled;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Profile getProfile() {
            return profile;
        }

        public void setProfile(Profile profile) {
            this.profile = profile;
        }
    }
}
