package com.zealcomm.base.entity;

public class LoginReq {


    /**
     * userName : zhy
     * pwd : zhy
     * role : customer
     * org : zhy
     */

    private String userName;
    private String pwd;
    private String role;
    private String org;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }
}
