package com.zealcomm.base.entity;

public class SessionData {

    private String media;
    private UserData userData;
    private ClientInfo clientInfo;

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public static class ClientInfo {
        private String type;

        public ClientInfo() {
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class UserData{

        private String formId;
        private String name;
        private String service;
        private String specifyAgent;
        private String media;

        public String getFormId() {
            return formId;
        }

        public void setFormId(String formId) {
            this.formId = formId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getSpecifyAgent() {
            return specifyAgent;
        }

        public void setSpecifyAgent(String specifyAgent) {
            this.specifyAgent = specifyAgent;
        }

        public String getMedia() {
            return media;
        }

        public void setMedia(String media) {
            this.media = media;
        }
    }

}
