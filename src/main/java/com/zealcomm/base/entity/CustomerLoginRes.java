package com.zealcomm.base.entity;

public class CustomerLoginRes {

    /**
     * code : 200
     * data : {"accessToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjVlYjYyYmFmMTlmOTFjMzRmNTNjNzZjYiIsIm9yZyI6IkphY2tpZSIsInVzZXJOYW1lIjoibGprIiwicm9sZSI6ImN1c3RvbWVyIiwiX192IjowLCJpZCI6IjVlYjYyYmFmMTlmOTFjMzRmNTNjNzZjYiJ9LCJpYXQiOjE1ODg5OTcwNDYsImV4cCI6MTU4OTA0MDI0Nn0.wihCcVvORIB1H9dBG7qFdwkE5_2x_O5xrjIloJiCunc","urls":{"uploadUrl":"https://52.83.72.175:3094","backendurl":"https://52.83.72.175:3014","ccsurl":"https://52.83.72.175:3021","turnserverUrl":"turn:106.14.116.204:443?transport=tcp","turnserverCredential":"woogeen","turnserverUsername":"purertc"}}
     */
    private int code;
    private DataBean data;
    /**
     * message : Internal Server Error
     * description : Password mistake
     */
    private String message;
    private String description;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class DataBean {
        /**
         * accessToken : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjVlYjYyYmFmMTlmOTFjMzRmNTNjNzZjYiIsIm9yZyI6IkphY2tpZSIsInVzZXJOYW1lIjoibGprIiwicm9sZSI6ImN1c3RvbWVyIiwiX192IjowLCJpZCI6IjVlYjYyYmFmMTlmOTFjMzRmNTNjNzZjYiJ9LCJpYXQiOjE1ODg5OTcwNDYsImV4cCI6MTU4OTA0MDI0Nn0.wihCcVvORIB1H9dBG7qFdwkE5_2x_O5xrjIloJiCunc
         * urls : {"uploadUrl":"https://52.83.72.175:3094","backendurl":"https://52.83.72.175:3014","ccsurl":"https://52.83.72.175:3021","turnserverUrl":"turn:106.14.116.204:443?transport=tcp","turnserverCredential":"woogeen","turnserverUsername":"purertc"}
         */

        private String accessToken;
        private UrlsBean urls;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public UrlsBean getUrls() {
            return urls;
        }

        public void setUrls(UrlsBean urls) {
            this.urls = urls;
        }

        public static class UrlsBean {
            /**
             * uploadUrl : https://52.83.72.175:3094
             * backendurl : https://52.83.72.175:3014
             * ccsurl : https://52.83.72.175:3021
             * turnserverUrl : turn:106.14.116.204:443?transport=tcp
             * turnserverCredential : woogeen
             * turnserverUsername : purertc
             */

            private String uploadUrl;
            private String backendurl;
            private String ccsurl;
            private String turnserverUrl;
            private String turnserverCredential;
            private String turnserverUsername;

            public String getUploadUrl() {
                return uploadUrl;
            }

            public void setUploadUrl(String uploadUrl) {
                this.uploadUrl = uploadUrl;
            }

            public String getBackendurl() {
                return backendurl;
            }

            public void setBackendurl(String backendurl) {
                this.backendurl = backendurl;
            }

            public String getCcsurl() {
                return ccsurl;
            }

            public void setCcsurl(String ccsurl) {
                this.ccsurl = ccsurl;
            }

            public String getTurnserverUrl() {
                return turnserverUrl;
            }

            public void setTurnserverUrl(String turnserverUrl) {
                this.turnserverUrl = turnserverUrl;
            }

            public String getTurnserverCredential() {
                return turnserverCredential;
            }

            public void setTurnserverCredential(String turnserverCredential) {
                this.turnserverCredential = turnserverCredential;
            }

            public String getTurnserverUsername() {
                return turnserverUsername;
            }

            public void setTurnserverUsername(String turnserverUsername) {
                this.turnserverUsername = turnserverUsername;
            }
        }
    }
}
