package com.zealcomm.base.entity;

public class UploadImageRes {


    /**
     * code : 200
     * data : {"url":"https://ivcs-demo.zealcomm.cn/upload/114cc3624c9ee9559675df6e7ef699fe.png","originalname":"zealcomm.png","filename":"114cc3624c9ee9559675df6e7ef699fe"}
     */

    private int code;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * url : https://ivcs-demo.zealcomm.cn/upload/114cc3624c9ee9559675df6e7ef699fe.png
         * originalname : zealcomm.png
         * filename : 114cc3624c9ee9559675df6e7ef699fe
         */

        private String url;
        private String originalname;
        private String filename;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getOriginalname() {
            return originalname;
        }

        public void setOriginalname(String originalname) {
            this.originalname = originalname;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }
    }
}
