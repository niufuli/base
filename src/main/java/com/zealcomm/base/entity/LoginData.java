package com.zealcomm.base.entity;

public class LoginData {


    /**
     * media : video
     * userData : {"service":"serviceA","specifyAgent":"agentName"}
     * clientInfo : {"type":"WeChat Mini Programs"}
     */

    private String media;
    private UserDataBean userData;
    private ClientInfoBean clientInfo;
    private String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public UserDataBean getUserData() {
        return userData;
    }

    public void setUserData(UserDataBean userData) {
        this.userData = userData;
    }

    public ClientInfoBean getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfoBean clientInfo) {
        this.clientInfo = clientInfo;
    }

    public static class UserDataBean {
        /**
         * service : serviceA
         * specifyAgent : agentName
         */

        private String service;
        private String specifyAgent;

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getSpecifyAgent() {
            return specifyAgent;
        }

        public void setSpecifyAgent(String specifyAgent) {
            this.specifyAgent = specifyAgent;
        }
    }

    public static class ClientInfoBean {
        /**
         * type : WeChat Mini Programs
         */

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
