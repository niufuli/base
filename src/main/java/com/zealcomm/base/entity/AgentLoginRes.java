package com.zealcomm.base.entity;

public class AgentLoginRes {


    /**
     * code : 200
     * data : {"accessToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjVlNzlkMjRkYzMwYzExMDAwZTVmN2I4OSIsIm9yZyI6IkphY2tpZSIsInVzZXJOYW1lIjoibGprIiwicm9sZSI6ImFnZW50IiwiX192IjowLCJpZCI6IjVlNzlkMjRkYzMwYzExMDAwZTVmN2I4OSJ9LCJpYXQiOjE1OTI1NjE5OTQsImV4cCI6MTU5MjYwNTE5NH0.8UoL3DHZYPZ5kVSGjacMbOl2L2QFMVNL6BEPb0emnBg","urls":{"uploadUrl":"https://ivcs-demo.zealcomm.cn:3094","backendurl":"https://ivcs-demo.zealcomm.cn","amsurl":"https://ivcs-demo.zealcomm.cn:3031","rtmpServer":"rtmp://52.83.39.63:1935/liveshow","turnserverUrl":"turn:106.14.116.204:443?transport=tcp","turnserverCredential":"woogeen","turnserverUsername":"purertc"}}
     */

    private int code;
    private DataBean data;
    /**
     * message : Internal Server Error
     * description : Password mistake
     */
    private String message;
    private String description;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * accessToken : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjVlNzlkMjRkYzMwYzExMDAwZTVmN2I4OSIsIm9yZyI6IkphY2tpZSIsInVzZXJOYW1lIjoibGprIiwicm9sZSI6ImFnZW50IiwiX192IjowLCJpZCI6IjVlNzlkMjRkYzMwYzExMDAwZTVmN2I4OSJ9LCJpYXQiOjE1OTI1NjE5OTQsImV4cCI6MTU5MjYwNTE5NH0.8UoL3DHZYPZ5kVSGjacMbOl2L2QFMVNL6BEPb0emnBg
         * urls : {"uploadUrl":"https://ivcs-demo.zealcomm.cn:3094","backendurl":"https://ivcs-demo.zealcomm.cn","amsurl":"https://ivcs-demo.zealcomm.cn:3031","rtmpServer":"rtmp://52.83.39.63:1935/liveshow","turnserverUrl":"turn:106.14.116.204:443?transport=tcp","turnserverCredential":"woogeen","turnserverUsername":"purertc"}
         */

        private String accessToken;
        private UrlsBean urls;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public UrlsBean getUrls() {
            return urls;
        }

        public void setUrls(UrlsBean urls) {
            this.urls = urls;
        }

        public static class UrlsBean {
            /**
             * uploadUrl : https://ivcs-demo.zealcomm.cn:3094
             * backendurl : https://ivcs-demo.zealcomm.cn
             * amsurl : https://ivcs-demo.zealcomm.cn:3031
             * rtmpServer : rtmp://52.83.39.63:1935/liveshow
             * turnserverUrl : turn:106.14.116.204:443?transport=tcp
             * turnserverCredential : woogeen
             * turnserverUsername : purertc
             */

            private String uploadUrl;
            private String backendurl;
            private String amsurl;
            private String rtmpServer;
            private String turnserverUrl;
            private String turnserverCredential;
            private String turnserverUsername;

            public String getUploadUrl() {
                return uploadUrl;
            }

            public void setUploadUrl(String uploadUrl) {
                this.uploadUrl = uploadUrl;
            }

            public String getBackendurl() {
                return backendurl;
            }

            public void setBackendurl(String backendurl) {
                this.backendurl = backendurl;
            }

            public String getAmsurl() {
                return amsurl;
            }

            public void setAmsurl(String amsurl) {
                this.amsurl = amsurl;
            }

            public String getRtmpServer() {
                return rtmpServer;
            }

            public void setRtmpServer(String rtmpServer) {
                this.rtmpServer = rtmpServer;
            }

            public String getTurnserverUrl() {
                return turnserverUrl;
            }

            public void setTurnserverUrl(String turnserverUrl) {
                this.turnserverUrl = turnserverUrl;
            }

            public String getTurnserverCredential() {
                return turnserverCredential;
            }

            public void setTurnserverCredential(String turnserverCredential) {
                this.turnserverCredential = turnserverCredential;
            }

            public String getTurnserverUsername() {
                return turnserverUsername;
            }

            public void setTurnserverUsername(String turnserverUsername) {
                this.turnserverUsername = turnserverUsername;
            }
        }
    }
}
