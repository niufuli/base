package com.zealcomm.base.entity;

public class MediaOptions {

    private VideoOptions videoOptions;

    public VideoOptions getVideoOptions() {
        return videoOptions;
    }

    public void setVideoOptions(VideoOptions videoOptions) {
        this.videoOptions = videoOptions;
    }

   public static class VideoOptions {

        private int videoWidth;
        private int videoHeight;
        private int videoFrameRate;

        public int getVideoWidth() {
            return videoWidth;
        }

        public void setVideoWidth(int videoWidth) {
            this.videoWidth = videoWidth;
        }

        public int getVideoHeight() {
            return videoHeight;
        }

        public void setVideoHeight(int videoHeight) {
            this.videoHeight = videoHeight;
        }

        public int getVideoFrameRate() {
            return videoFrameRate;
        }

        public void setVideoFrameRate(int videoFrameRate) {
            this.videoFrameRate = videoFrameRate;
        }
    }


}
