package com.zealcomm.base.entity;

public class Profile {
    private boolean isRobot;
    private String picture;
    private String fullName;
    private String gender;

    public boolean getIsRobot() {
        return isRobot;
    }

    public void setIsRobot(boolean isRobot) {
        this.isRobot = isRobot;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String fullName) {
        this.gender = gender;
    }
}
