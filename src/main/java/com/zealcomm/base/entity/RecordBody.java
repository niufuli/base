package com.zealcomm.base.entity;

public class RecordBody {


    /**
     * container : auto
     * media : {"audio":"audio","video":{"from":"videostreamId"}}
     */

    private String container;
    private MediaBean media;

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public MediaBean getMedia() {
        return media;
    }

    public void setMedia(MediaBean media) {
        this.media = media;
    }

    public static class MediaBean {
        /**
         * audio : audio
         * video : {"from":"videostreamId"}
         */

        private AudioBean audio;
        private VideoBean video;

        public AudioBean getAudio() {
            return audio;
        }

        public void setAudio(AudioBean audio) {
            this.audio = audio;
        }

        public VideoBean getVideo() {
            return video;
        }

        public void setVideo(VideoBean video) {
            this.video = video;
        }

        public static class AudioBean {
            /**
             * from : videostreamId
             */

            private String from;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }
        }

        public static class VideoBean {
            /**
             * from : videostreamId
             */

            private String from;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }
        }
    }
}
