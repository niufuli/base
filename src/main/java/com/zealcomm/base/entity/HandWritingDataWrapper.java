package com.zealcomm.base.entity;

public class HandWritingDataWrapper {

    private HandWritingData nameValuePairs;

    public static class HandWritingData {
        private String from;
        private String id;
        private String to;
        private String type;
        private String data;

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public HandWritingData getNameValuePairs() {
        return nameValuePairs;
    }

    public void setNameValuePairs(HandWritingData nameValuePairs) {
        this.nameValuePairs = nameValuePairs;
    }
}
