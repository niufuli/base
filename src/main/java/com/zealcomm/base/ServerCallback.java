package com.zealcomm.base;

public interface ServerCallback {

    void successResponse(String result) throws Exception;

    void failedResponse(String result);



}
