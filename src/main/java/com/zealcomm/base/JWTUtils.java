package com.zealcomm.base;

import android.util.Base64;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

public class JWTUtils {

    public static <T> T decoded(String jwtEncoded,Class<T> clazz){
        String body = null;
        try {
            String[] split = jwtEncoded.split("\\.");
            body = getJson(split[1]);
        } catch (UnsupportedEncodingException e) {
            //Error
        }
        return body == null ? null : new Gson().fromJson(body,clazz);
    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException{
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }

}
