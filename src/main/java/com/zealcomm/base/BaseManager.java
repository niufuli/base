package com.zealcomm.base;

import android.content.Context;

import com.zealcomm.base.entity.GroupData;

import java.util.List;
import java.util.concurrent.ExecutorService;

public class BaseManager {

    public String TAG = this.getClass().getName();
    public Context mContext;
    public ExecutorService mExecutorService;
    public static final long DURATION = 200L;
    public String backendUrl = "https://ivcs-demo.zealcomm.cn";
    public String mName, mAgency;
    public String mToken;
    public static final int DEFAULT_PUBLISH_VIDEO_WIDTH = 640;
    public static final int DEFAULT_PUBLISH_VIDEO_HEIGHT = 480;
    public static final int DEFAULT_PUBLISH_VIDEO_FPS = 15;
    public static final int DEFAULT_VIDEO_MAX_BITRATE = 512;
    public final String MEDIA = "video";
    public final String CLIENT_INFO = "android";
    public static final String LOCAL_RENDER = "local";
    public String mStreamType;
    public InitCallBack mInitCallback;

    public interface InitCallBack {
        void initSuccess(String token, List<String> groupIds, List<GroupData> groupIdList);
        void initFailed(int code,String error);
    }
}
