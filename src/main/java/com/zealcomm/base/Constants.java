package com.zealcomm.base;

public class Constants {

    public static final String STREAM_TYPE = "type";
    public static final String STREAM_TYPE_SCREEN = "screen";
    public static final String STREAM_TYPE_CUSTOMER = "customer";
    public static final String STREAM_TYPE_AGENT = "agent";
    public static final String STREAM_TYPE_AI = "ai";

    public static final String PEER_STATUS = "peer-status";
    public static final String USER_CUSTOMERS = "customers";
    public static final String READY_TO_TALK = "ready-to-talk";
    public static final String MESSAGE_CONFIRMATION = "message-confirmation";

    public static final int HTTP_REQUEST_SUCCESS_CODE = 200;

    public static final String SIGNATURE = "Signature";
    public static final String INPUT_FORM_SUCCESS = "inputformsuccess";


    /***
     * 代表发的消息内容类型
     */
    public static final String LINK_DOC = "doc";
    public static final String LINK_PIC = "pic";
    public static final String LINK_VIDEO= "video";
    public static final String LINK_AUDIO = "audio";
    public static final String LINK_PAGE= "page";
    public static final String LINK_PAPER= "paper";

    /***
     * 代表客户端类型
     */
    public static final String CLIENT_AGENT = "agents";
    public static final String CLIENT_CUSTOMERS = "customers";

    public static final String ACK_MESSAGE_TYPE = "ack";
    public static final String SIGNATURE_MESSAGE = "signature_message";

}
