package com.zealcomm.base;

import android.util.Log;

import java.security.MessageDigest;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class HmacShaUtils {


    public static String hmacSha1(String key, String value) {
        return hmacSha(key, value, "HmacSHA1");
    }

    public static String hmacSha256(String key, String value) {
        return hmacSha(key, value, "HmacSHA256");
    }

    private static String hmacSha(String key, String value, String shaType) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes("UTF-8"), shaType);
            Mac mac = Mac.getInstance(shaType);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(value.getBytes("UTF-8"));
            String result = byteArrayToHexString(rawHmac);
            Log.i("hmacSha","hexChars = "+result);
//            String result = Base64.encodeToString(hexChars.getBytes(),Base64.NO_WRAP);
            return result;
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    public  static String byteArrayToHexString(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp;
        for (int n = 0; b!=null && n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0XFF);
            if (stmp.length() == 1) {
                hs.append('0');
            }
            hs.append(stmp);
        }
        return hs.toString().toLowerCase();
    }

    public static String digest(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] bytes = digest.digest(password.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                int c = b & 0xff;
                String result = Integer.toHexString(c);
                if(result.length()<2){
                    sb.append(0);
                }
                sb.append(result);
            }
            return sb.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

}
