package com.zealcomm.base;

import android.content.Context;
import android.text.TextUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtils {

    private static Retrofit mSingleton;
    private static int CONNECT_TIMEOUT = 60 * 3;
    private static int MAX_RETRY = 5;

    public static <T> T createApi(Context context, Class<T> clazz, String url) {
        if (mSingleton == null) {
            synchronized (RetrofitUtils.class) {
                if (mSingleton == null) {
                    Retrofit.Builder builder = new Retrofit.Builder();
                    builder.baseUrl(url)
                            .client(getHeaderOkHttpClient(context))
                            .addConverterFactory(GsonConverterFactory.create());

                    mSingleton = builder.build();
                }
            }
        }
        return mSingleton.create(clazz);
    }

    static OkHttpClient getHeaderOkHttpClient(final Context context) {
        OkHttpClient.Builder builder = null;
        builder = new OkHttpClient.Builder()
                .sslSocketFactory(TLSSocketFactory.delegate, TLSSocketFactory.DEFAULT_TRUST_MANAGERS)
                .hostnameVerifier(TLSSocketFactory.mHostnameVerifier)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(new ChangeUrlInterceptor())
                .addInterceptor(new Retry(MAX_RETRY));
        builder.addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        OkHttpClient client = builder.build();
        return client;
    }

    private static class ChangeUrlInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            // 获得初始的请求
            Request originalReq = chain.request();
            // 获得初始的地址
            HttpUrl originalUrl = originalReq.url();
            // 获取新的 baseUrl，一般情况，这里控制 headValues 中至多只有一个 new_base_url
            String newBaseUrlTag = originalReq.header("new_base_url_tag");
            if (TextUtils.isEmpty(newBaseUrlTag)) {
                // 还是用已经配置的 url
            } else {
                // 当有多个 baseUrl 时可以考虑用 map ，这里先直接判断了
                if (newBaseUrlTag.equals("test")) {
                    String newBaseUrl = "https://www.baidu.com";
                    // 构造一个新的请求：
                    Request newReq = originalReq
                            // 先通过 originalReq 复制一份信息完全相同的 builder
                            .newBuilder()
                            // 把正常请求之外的 head 参数删掉
                            .removeHeader("new_base_url_tag")
                            // 填写上新的 url
                            .url(newBaseUrl)
                            .build();
                    return chain.proceed(newReq);
                }
            }
            return chain.proceed(originalReq);
        }
    }
}
