package com.zealcomm.base;

public class HttpErrorCode {

    public static final int NETWORK_ERROR = 1400;
    public static final int PASSWORD_ERROR = 401;
    public static final int AMS_ERROR = 1402;
    public static final int CCS_ERROR = 1403;
    public static final int ZMS_ERROR = 1404;
    public static final int UNKNOW_ERROR = 1405;

    public static final int SEND_IMAGE_ERROR = 1406;
    public static final int CCS_LOGIN_ERROR = 1407;
    public static final int CCS_QUIT_ERROR = 1408;
    public static final int SEND_LINK_EOORO = 1409;
    public static final int SEND_TEXT_EOORO = 1409;
    public static final int SEND_FORM_EOORO = 1410;

    public static final int AMS_CONNECT_ERROR = 1411;
    public static final int AMS_LOGIN_ERROR = 1412;
    public static final int AMS_QUIT_ERROR = 1413;
    public static final int AMS_READY_ERROR = 1414;
    public static final int AMS_ANSWER_ERROR = 1415;
    public static final int INVITE_USER_ERROR = 1416;
    public static final int REQUEST_SESSION_ERROR = 1417;
    public static final int AMS_CHECK_IN_ERROR = 1418;
    public static final int AMS_HANGUP_ERROR = 1419;
    public static final int AMS_CHECK_OUT_ERROR = 1420;

    public static final int CCS_COMPARE_FACE_ERROR = 1421;


    public static final String NETWORK_REQUEST_FAILED  = "Network error";
    public static final String CCS_SERVER_CONNECT_ERROR  = "Ccs server connect error";
    public static final String CCS_LOGIN_ERROR_MESSAGE = "Ccs client login error";
    public static final String CCS_QUIT_ERROR_MESSAGE = "Ccs client quit error";
    public static final String SEND_IMAGE_ERROR_MESSAGE = "send image error";
    public static final String SEND_LINK_ERROR_MESSAGE = "send link error";
    public static final String SEND_TEXT_ERROR_MESSAGE = "send text error";
    public static final String SEND_FORM_ERROR_MESSAGE = "send form error";

    public static final String AMS_SERVER_CONNECT_ERROR  = "Ams server connect error";
    public static final String AMS_LOGIN_ERROR_MESSAGE = "Ams client login error";
    public static final String AMS_QUIT_ERROR_MESSAGE = "Ams client quit error";
    public static final String AMS_READY_ERROR_MESSAGE = "Ams client ready error";
    public static final String AMS_ANSWER_ERROR_MESSAGE = "Ams client answer error";
    public static final String INVITE_USER_ERROR_MESSAGE = "invite user error";
    public static final String REQUEST_SESSION_ERROR_MESSAGE = "not found";
    public static final String AMS_CHECK_IN_ERROR_MESSAGE = "Ams client check in error";
    public static final String AMS_HANGUP_ERROR_MESSAGE = "Ams client hangup error" ;
    public static final String AMS_CHECK_OUT_ERROR_MESSAGE = "Ams client check out error";

    public static final String ZMS_PUBLISH_ERROR_MESSAGE = "Zms publish error";
}
